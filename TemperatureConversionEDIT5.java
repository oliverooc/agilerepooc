
public class TemperatureConversionEDIT5 {

	public double CelsiusToFahrenheit(double celsiusVal) throws ConversionExceptionHandlerEDIT1 {

		if ((celsiusVal < -273.15) || (celsiusVal > 100.00)) {

			throw new ConversionExceptionHandlerEDIT1("Incorrect values");
		}

		else {

			return (celsiusVal * 9 / 5) + 32;
		}
	}

	public double FarenheitToCelsius(double fahrenheitVal) throws ConversionExceptionHandlerEDIT1 {

		if ((fahrenheitVal < -459.67) || (fahrenheitVal > 212.00)) {

			throw new ConversionExceptionHandlerEDIT1("Incorrect values");
		}

		else {

			return (fahrenheitVal - 32) * 5 / 9;

		}
	}

}
