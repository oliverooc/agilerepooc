import junit.framework.TestCase;

public class TemperatureConversionEDIT9Test extends TestCase {
	
	/*
	 Test #: 1
	 Test Objective: to convert a celsius value that is below the absolute minimum boundary value
	 Test Inputs: val = 4.9E-325
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit01()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(4.9E-325);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 1
	
	
	/*
	 Test #: 2
	 Test Objective: to convert a celsius value that is the absolute minimum boundary value
	 Test Inputs: val = 4.9E-324
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit02()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(4.9E-324);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 2
	
	
	/*
	 Test #: 3
	 Test Objective: to convert a celsius value that is above the absolute minimum boundary value
	 Test Inputs: val = 4.9E-326
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit03()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(4.9E-323);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 3
	
	
	
	
	/*
	 Test #: 4
	 Test Objective: to convert a celsius value that is below the minimum boundary value
	 Test Inputs: val = -273.16
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit04()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(-273.16);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 4
	
	
	/*
	 Test #: 5
	 Test Objective: to convert a celsius value that is the minimum boundary value 				
	 Test Inputs: val = -273.15
	 Expected Output: fahrenheit = -459.67
	 */
	
	public void testCelsiusToFahrenheit05(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(-273.15);
		assertEquals(-459.67,fahrenheit);
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
} // end test 5
	
	
	
	/*
	 Test #: 6
	 Test Objective: to convert a celsius value that is above the minimum boundary value
	 Test Inputs: val = -273.14
	 Expected Output: fahrenheit = -459.65
	 */
	
	public void testCelsiusToFahrenheit06(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(-273.14);
		assertEquals(-459.65,fahrenheit);
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
} // end test 6
	
	

	/*
	 Test #: 7
	 Test Objective: to convert a celsius value that is below the nominal boundary value
	 Test Inputs: val = -0.01
	 Expected Output: fahrenheit = 31.98
	 */

	public void testCelsiusToFahrenheit07(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(-0.01);
		assertEquals(31.98, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
	} // end test 7
	
	
	
	/*
	 Test #: 8
	 Test Objective: to convert a celsius value that is the nominal boundary value
	 Test Inputs: val = 0.00
	 Expected Output: fahrenheit = 32
	 */

	public void testCelsiusToFahrenheit08(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(0.00);
		assertEquals(32, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
	
	} // end test 8
	
	
	
	/*
	 Test #: 9
	 Test Objective: to convert a celsius value that is above the nominal boundary value
	 Test Inputs: val = 0.01
	 Expected Output: fahrenheit = 32.02
	 */

	public void testCelsiusToFahrenheit09(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(0.01);
		assertEquals(32.02, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	} // end test 9
	
	
	

	/*
	 Test #: 10
	 Test Objective: to convert a celsius value that is below the maximum value
	 Test Inputs: val = 99.99
	 Expected Output: fahrenheit = 211.98
	 */

	public void testCelsiusToFahrenheit10(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(99.99);
		assertEquals(211.98, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	} // end test 10
	
	
	/*
	 Test #: 11
	 Test Objective: to convert a celsius value that is the maximum value
	 Test Inputs: val = 100.00
	 Expected Output: fahrenheit = 212
	 */

	public void testCelsiusToFahrenheit11(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(100.00);
		assertEquals(212, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	} // end test 11
	
	
	/*
	 Test #: 12
	 Test Objective: to convert a celsius value that is above the maximum value
	 Test Inputs: val = 100.01
	 Expected Output: an exception should occur, stating that invalid values were specified
	 */

	public void testCelsiusToFahrenheit12(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(100.01);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	} // end test 12
	
	
	
	/*
	 Test #: 13
	 Test Objective: to convert a celsius value that is below the absolute maximum boundary value
	 Test Inputs: val = 1.7976931348623156E308
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit13()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(1.7976931348623156E308);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 13
	
	
	/*
	 Test #: 14
	 Test Objective: to convert a celsius value that is the absolute maximum boundary value
	 Test Inputs: val = 1.7976931348623157E308
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit14()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(1.7976931348623157E308);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 14
	
	
	/*
	 Test #: 15
	 Test Objective: to convert a celsius value that is above the absolute maximum boundary value
	 Test Inputs: val = 1.7976931348623159E308
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit15()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(1.7976931348623159E308);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 15
	
}
// =========================================================================================//
//																						   //
// ========================================================================================//
		


/*
Test #: 1
Test Objective: to convert a fahrenehit value that is below the absolute minimum boundary value
Test Inputs: val = 4.9E-325
Expected Output: an exception should occur, stating that invalid values were specified
 */


public void testFarenheitToCelsius01()
{
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double res = tempcon.FarenheitToCelsius(4.9E-325);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
} // end test 1


/*
Test #: 2
Test Objective: to convert a fahrenehit value that is the absolute minimum boundary value
Test Inputs: val = 4.9E-324
Expected Output: an exception should occur, stating that invalid values were specified
 */


public void testFarenheitToCelsius02()
{
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double res = tempcon.FarenheitToCelsius(4.9E-324);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
} // end test 2


/*
Test #: 3
Test Objective: to convert a fahrenehit value that is above the absolute minimum boundary value
Test Inputs: val = 4.9E-323
Expected Output: an exception should occur, stating that invalid values were specified
 */


public void testFarenheitToCelsius03()
{
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double res = tempcon.FarenheitToCelsius(4.9E-323);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
} // end test 3




/*
Test #: 4
Test Objective: to convert a fahrenehit value that is below the minimum boundary value
Test Inputs: val = -459.68
Expected Output: an exception should occur, stating that invalid values were specified
 */


public void testFarenheitToCelsius04()
{
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double res = tempcon.FarenheitToCelsius(-459.68);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
} // end test 4




/*
Test #: 5
Test Objective: to convert a fahrenheit value that is the minimum boundary value 				
Test Inputs: val = -459.67
Expected Output: celsius = -273.15
*/

public void testFarenheitToCelsius05(){
TemperatureConversion tempcon = new TemperatureConversion();

try{
	double celsius = tempcon.FarenheitToCelsius(-459.67);
	assertEquals(-273.15,celsius);
}

catch(ConversionExceptionHandler conversionExceptionHandler){
	assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
}


} // end test 5



/*
Test #: 6
Test Objective: to convert a fahreneheit value that is above the minimum boundary value
Test Inputs: val = -459.66
Expected Output: celsius = -273.14
*/

public void testFarenheitToCelsius06(){
TemperatureConversion tempcon = new TemperatureConversion();

try{
	double fahrenheit = tempcon.FarenheitToCelsius(-459.66);
	assertEquals(-273.14,fahrenheit);
}

catch(ConversionExceptionHandler conversionExceptionHandler){
	assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
}


} // end test 6



/*
Test #: 7
Test Objective: to convert a fahrenheit value that is below the nominal boundary value
Test Inputs: val = -0.01
Expected Output: celsius = -17.78
*/

public void testFarenheitToCelsius07(){
TemperatureConversion tempcon = new TemperatureConversion();

try{
	double celsius = tempcon.FarenheitToCelsius(-0.01);
	assertEquals(-17.78, celsius);
	
}

catch(ConversionExceptionHandler conversionExceptionHandler){
	assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
}


} // end test 7



/*
Test #: 8
Test Objective: to convert a fahrenheit value that is the nominal boundary value
Test Inputs: val = 0.00
Expected Output: celsius = -17.78
*/

public void testFarenheitToCelsius08(){
TemperatureConversion tempcon = new TemperatureConversion();

try{
	double celsius = tempcon.FarenheitToCelsius(0.00);
	assertEquals(-17.78, celsius);
	
}

catch(ConversionExceptionHandler conversionExceptionHandler){
	assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
}



} // end test 8



/*
Test #: 9
Test Objective: to convert a fahrenheit value that is above the nominal boundary value
Test Inputs: val = 0.01
Expected Output: celsius = -17.77
*/

public void testFarenheitToCelsius09(){
TemperatureConversion tempcon = new TemperatureConversion();

try{
	double celsius = tempcon.FarenheitToCelsius(0.01);
	assertEquals(-17.77, celsius);
	
}

catch(ConversionExceptionHandler conversionExceptionHandler){
	assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
}

} // end test 9




/*
Test #: 10
Test Objective: to convert a fahrenheit value that is below the maximum value
Test Inputs: val = 211.99
Expected Output: celsius = 99.99 
*/

public void testFarenheitToCelsius10(){
TemperatureConversion tempcon = new TemperatureConversion();

try{
	double celsius = tempcon.FarenheitToCelsius(211.99);
	assertEquals(99.99, celsius);
	
}

catch(ConversionExceptionHandler conversionExceptionHandler){
	assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
}

} // end test 10


/*
Test #: 11
Test Objective: to convert a fahreneheit value that is the maximum value
Test Inputs: val = 212.00
Expected Output: celsius = 100
*/

public void testFarenheitToCelsius11(){
TemperatureConversion tempcon = new TemperatureConversion();

try{
	double fahrenheit = tempcon.FarenheitToCelsius(212.00);
	assertEquals(100, fahrenheit);
	
}

catch(ConversionExceptionHandler conversionExceptionHandler){
	assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
}

} // end test 11



/*
Test #: 12
Test Objective: to convert a fahrenheit value that is above the maximum value
Test Inputs: val = 212.01
Expected Output: an exception should occur, stating that invalid values were specified
*/

public void testFarenheitToCelsius12(){
TemperatureConversion tempcon = new TemperatureConversion();

try{
	double celsius = tempcon.FarenheitToCelsius(212.01);
	fail("Exception should occur...");
	
}

catch(ConversionExceptionHandler conversionExceptionHandler){
	assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
}

} // end test 12



/*
Test #: 13
Test Objective: to convert a fahrenheit value that is below the absolute maximum boundary value
Test Inputs: val = 1.7976931348623156E308
Expected Output: an exception should occur, stating that invalid values were specified
 */


public void testFarenheitToCelsius13()
{
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double res = tempcon.FarenheitToCelsius(1.7976931348623156E308);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
} // end test 13


/*
Test #: 14
Test Objective: to convert a fahrenheit value that is the absolute maximum boundary value
Test Inputs: val = 1.7976931348623157E308
Expected Output: an exception should occur, stating that invalid values were specified
 */


public void testFarenheitToCelsius14()
{
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double res = tempcon.FarenheitToCelsius(1.7976931348623157E308);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
} // end test 14


/*
Test #: 15
Test Objective: to convert a fahrenheit value that is above the absolute maximum boundary value
Test Inputs: val = 1.7976931348623159E308
Expected Output: an exception should occur, stating that invalid values were specified
 */


public void testFarenheitToCelsius15()
{
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double res = tempcon.FarenheitToCelsius(1.7976931348623159E308);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
} // end test 15


