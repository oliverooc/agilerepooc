import junit.framework.TestCase;

public class TemperatureConversionTest extends TestCase {
	
	/*
	 Test #: 1
	 Test Objective: to convert a celsius value that is below absolute zero
	 Test Inputs: val = -273.14
	 Expected Output: an exception will occur
	 */
	
	/*
	 Test #: 2
	 Test Objective: to convert a celsius value that is greater than absolute zero
	 Test Inputs: val = 273.16
	 Expected Output: the temperature in fahrenheit 
	 */

}
