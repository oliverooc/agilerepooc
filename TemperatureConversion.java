public class TemperatureConversion {

	public double CelsiusToFahrenheit(double celsiusVal)
			throws ConversionExceptionHandler {

		if ((celsiusVal < -273.15) || (celsiusVal > 100.00)) {

			throw new ConversionExceptionHandler("Incorrect values");
		}

		else {
			double result = (celsiusVal * 9 / 5) + 32;
			double fahrenheit = Math.round(result * 100);
			fahrenheit /= 100;

			return fahrenheit;
		}
	}

	public double FarenheitToCelsius(double fahrenheitVal)
			throws ConversionExceptionHandler {

		if ((fahrenheitVal < -459.67) || (fahrenheitVal > 212.00)) {

			throw new ConversionExceptionHandler("Incorrect values");
		}

		else {

			double result = (fahrenheitVal - 32) * 5 / 9;
			double celsius = Math.round(result * 100);
			celsius /= 100;

			return celsius;

		}
	}

}
