
public class TemperatureConversionEDIT3 {
	
	public double CelsiusToFahrenheit(double celsiusVal){
		
		if( (celsiusVal <-273.15)  || (celsiusVal >100.00) )
			{
			
				System.out.println("error");
			}
		
		return (celsiusVal * 9/5) + 32;
		
	}
	
	public double FarenheitToCelsius(double fahrenheitVal){
		
		if( (fahrenheitVal <-459.67)  || (fahrenheitVal >212.00) )
		{
		
			System.out.println("error");
		}
		
	
		return (fahrenheitVal - 32) * 5 / 9;
		
	}

}
