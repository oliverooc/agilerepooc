import junit.framework.TestCase;

public class TemperatureConversionEDIT5Test extends TestCase {
	
	/*
	 Test #: 1
	 Test Objective: to convert a celsius value that is below absolute zero
	 Test Inputs: val = -273.16
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit001()
	{
		TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(-273.16);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	}
	
	
	
	/*
	 Test #: 2
	 Test Objective: to convert a celsius value that is 
	 				 within the valid range of -273.15 to 100 degrees celsius 
	 Test Inputs: val = -273.14
	 Expected Output: fahrenheit = -459.67
	 */
	
	public void testCelsiusToFahrenheit002(){
	TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
	
	try{
		double calc = tempcon.CelsiusToFahrenheit(-273.14);
		double res = Math.round(calc*100);
		res /= 100;
		assertEquals(-459.65,res);
	}
	
	catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
}


	
// this is a method which was incorrectly implemented, thus causing errors
//	public void testCelsiusToFahrenheit002(){
//		TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
//		
//		try{
//			assertEquals(-459.65, tempcon.CelsiusToFahrenheit(-273.14));
//		}
//		
//		catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
//			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
//		}
//		
//		
//	}
	
	

	/*
	 Test #: 3
	 Test Objective: to convert a celsius value that is greater than absolute zero
	 Test Inputs: val = 100.01
	 Expected Output: an exception should occur, stating that invalid values were specified
	 */

	public void testCelsiusToFahrenheit003(){
	TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
	
	try{
		double res = tempcon.CelsiusToFahrenheit(100.01);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
	}
}
