import junit.framework.TestCase;

public class TemperatureConversionEDIT6Test extends TestCase {
	
	/*
	 Test #: 1
	 Test Objective: to convert a celsius value that is below absolute zero
	 Test Inputs: val = -273.17
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit001()
	{
		TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(-273.17);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	}
	
	
	
	/*
	 Test #: 2
	 Test Objective: to convert a celsius value that is 
	 				 within the valid range of -273.15 to 100 degrees celsius 
	 Test Inputs: val = -273.14
	 Expected Output: fahrenheit = -459.65
	 */
	
	public void testCelsiusToFahrenheit002(){
	TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
	
	try{
		double calc = tempcon.CelsiusToFahrenheit(-273.14);
		double res = Math.round(calc*100);
		res /= 100;
		assertEquals(-459.65,res);
	}
	
	catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
}



	/*
	 Test #: 3
	 Test Objective: to convert a celsius value that is greater than the 100 degree range limit
	 Test Inputs: val = 100.02
	 Expected Output: an exception should occur, stating that invalid values were specified
	 */

	public void testCelsiusToFahrenheit003(){
	TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
	
	try{
		double res = tempcon.CelsiusToFahrenheit(100.02);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
	}

	
		/*
		 Test #: 4
		 Test Objective: to convert a farenheit value that is below absolute zero
		 Test Inputs: val = -459.69
		 Expected Output: an exception should occur, stating that invalid values were specified
		  */
		
		
		public void testFarenheitToCelsius001()
		{
			TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
			
			try{
				double res = tempcon.FarenheitToCelsius(-459.69);
				fail("Exception should occur...");
				
			}
			
			catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
				assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
			}
			
		}
		
		
		
		/*
		 Test #: 5
		 Test Objective: to convert a fahrenheit value that is 
		 				 within the valid range of -459.67 to 212 degrees fahrenheit 
		 Test Inputs: val = -459.66
		 Expected Output: celsius = -273.14
		 */
	
		public void testFarenheitToCelsius002(){
		TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
		
		try{
			double calc = tempcon.FarenheitToCelsius(-459.66);
			double res = Math.round(calc*100);
			res /= 100;
			assertEquals(-273.14,res);
		}
		
		catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
		
	}


		/*
		 Test #: 6
		 Test Objective: to convert a fahrenheit value that is greater than the 212 degree range limit
		 Test Inputs: val = 212.02
		 Expected Output: an exception should occur, stating that invalid values were specified
		 */

		public void testFarenheitToCelsius003(){
		TemperatureConversionEDIT5 tempcon = new TemperatureConversionEDIT5();
		
		try{
			double res = tempcon.FarenheitToCelsius(212.02);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
		
		}
//	}

}
