
public class ConversionExceptionHandler extends Exception {
	
	String infoMessage;
	
	public ConversionExceptionHandler(String errorMessage){
		infoMessage = errorMessage;
	}
	
	public String getInfoMessage(){
		return infoMessage;
	}

}
