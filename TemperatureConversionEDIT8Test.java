import junit.framework.TestCase;

public class TemperatureConversionEDIT8Test extends TestCase {
	
	/*
	 Test #: 1
	 Test Objective: to convert a celsius value that is below the absolute minimum boundary value
	 Test Inputs: val = 4.9E-325
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit001()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(4.9E-325);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 1
	
	
	/*
	 Test #: 2
	 Test Objective: to convert a celsius value that is the absolute minimum boundary value
	 Test Inputs: val = 4.9E-324
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit002()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(4.9E-324);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 2
	
	
	/*
	 Test #: 3
	 Test Objective: to convert a celsius value that is above the absolute minimum boundary value
	 Test Inputs: val = 4.9E-323
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit003()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(4.9E-323);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 3
	
	
	
	
	/*
	 Test #: 4
	 Test Objective: to convert a celsius value that is below the minimum boundary value
	 Test Inputs: val = -273.16
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit004()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(-273.16);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 4
	
	
	/*
	 Test #: 5
	 Test Objective: to convert a celsius value that is the minimum boundary value 				
	 Test Inputs: val = -273.15
	 Expected Output: fahrenheit = -459.67
	 */
	
	public void testCelsiusToFahrenheit005(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(-273.15);
		assertEquals(-459.67,fahrenheit);
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
} // end test 5
	
	
	
	/*
	 Test #: 6
	 Test Objective: to convert a celsius value that is above the minimum boundary value
	 Test Inputs: val = -273.14
	 Expected Output: fahrenheit = -459.65
	 */
	
	public void testCelsiusToFahrenheit006(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(-273.14);
		assertEquals(-459.65,fahrenheit);
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
} // end test 6
	
	

	/*
	 Test #: 7
	 Test Objective: to convert a celsius value that is below the nominal boundary value
	 Test Inputs: val = -0.01
	 Expected Output: fahrenheit = 31.98
	 */

	public void testCelsiusToFahrenheit007(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(-0.01);
		assertEquals(31.98, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
	} // end test 7
	
	
	
	/*
	 Test #: 8
	 Test Objective: to convert a celsius value that is the nominal boundary value
	 Test Inputs: val = 0.00
	 Expected Output: fahrenheit = 32
	 */

	public void testCelsiusToFahrenheit008(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(0.00);
		assertEquals(32, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	
	
	} // end test 8
	
	
	
	/*
	 Test #: 9
	 Test Objective: to convert a celsius value that is above the nominal boundary value
	 Test Inputs: val = 0.01
	 Expected Output: fahrenheit = 32.02
	 */

	public void testCelsiusToFahrenheit009(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(0.01);
		assertEquals(32.02, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	} // end test 9
	
	
	

	/*
	 Test #: 10
	 Test Objective: to convert a celsius value that is below the maximum value
	 Test Inputs: val = 99.99
	 Expected Output: fahrenheit = 211.98
	 */

	public void testCelsiusToFahrenheit010(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(99.99);
		assertEquals(211.98, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	} // end test 10
	
	
	/*
	 Test #: 11
	 Test Objective: to convert a celsius value that is the maximum value
	 Test Inputs: val = 100.00
	 Expected Output: fahrenheit = 212
	 */

	public void testCelsiusToFahrenheit011(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(100.00);
		assertEquals(212, fahrenheit);
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	} // end test 11
	
	
	/*
	 Test #: 12
	 Test Objective: to convert a celsius value that is above the maximum value
	 Test Inputs: val = 100.01
	 Expected Output: an exception should occur, stating that invalid values were specified
	 */

	public void testCelsiusToFahrenheit012(){
	TemperatureConversion tempcon = new TemperatureConversion();
	
	try{
		double fahrenheit = tempcon.CelsiusToFahrenheit(100.01);
		fail("Exception should occur...");
		
	}
	
	catch(ConversionExceptionHandler conversionExceptionHandler){
		assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
	}
	
	} // end test 12
	
	
	
	/*
	 Test #: 13
	 Test Objective: to convert a celsius value that is below the absolute maximum boundary value
	 Test Inputs: val = 1.7976931348623156E308
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit013()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(1.7976931348623156E308);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 13
	
	
	/*
	 Test #: 14
	 Test Objective: to convert a celsius value that is the absolute maximum boundary value
	 Test Inputs: val = 1.7976931348623157E308
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit014()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(1.7976931348623157E308);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 14
	
	
	/*
	 Test #: 15
	 Test Objective: to convert a celsius value that is above the absolute maximum boundary value
	 Test Inputs: val = 1.7976931348623159E308
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit015()
	{
		TemperatureConversion tempcon = new TemperatureConversion();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(1.7976931348623159E308);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandler conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	} // end test 15
	
}
// =========================================================================================//
//																						   //
// ========================================================================================//
		



