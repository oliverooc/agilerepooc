
public class ConversionExceptionHandlerEDIT1 extends Exception {
	
	String infoMessage;
	
	public ConversionExceptionHandlerEDIT1(String errorMessage){
		infoMessage = errorMessage;
	}
	
	public String getInfoMessage(){
		return infoMessage;
	}

}
