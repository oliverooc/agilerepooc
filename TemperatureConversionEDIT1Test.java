import junit.framework.TestCase;

public class TemperatureConversionEDIT1Test extends TestCase {
	
	/*
	 Test #: 1
	 Test Objective: to convert a celsius value that is below absolute zero
	 Test Inputs: val = -273.14
	 Expected Output: an exception occurs
	 */
	
	/*
	 Test #: 2
	 Test Objective: to convert a celsius value that is greater than absolute zero
	 Test Inputs: val = -273.15
	 Expected Output: the temperature in fahrenheit will be displayed
	 */

}
