import junit.framework.TestCase;

public class TemperatureConversionEDIT3Test extends TestCase {
	
	/*
	 Test #: 1
	 Test Objective: to convert a celsius value that is below absolute zero
	 Test Inputs: val = -273.14
	 Expected Output: an error should occur
	  */
	
	
	public void testCelsiusToFahrenheit001()
	{
		TemperatureConversionEDIT3 tempcon = new TemperatureConversionEDIT3();
		
		double result = tempcon.CelsiusToFahrenheit(-273.14);
		
		fail("error");
	}
	
	/*
	 Test #: 2
	 Test Objective: to convert a celsius value that is greater than absolute zero
	 Test Inputs: val = -273.15
	 Expected Output: the temperature in fahrenheit will be displayed
	 */

}
