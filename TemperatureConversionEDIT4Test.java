import junit.framework.TestCase;

public class TemperatureConversionEDIT4Test extends TestCase {
	
	/*
	 Test #: 1
	 Test Objective: to convert a celsius value that is below absolute zero
	 Test Inputs: val = -273.16
	 Expected Output: an exception should occur, stating that invalid values were specified
	  */
	
	
	public void testCelsiusToFahrenheit001()
	{
		TemperatureConversionEDIT4 tempcon = new TemperatureConversionEDIT4();
		
		try{
			double res = tempcon.CelsiusToFahrenheit(-273.16);
			fail("Exception should occur...");
			
		}
		
		catch(ConversionExceptionHandlerEDIT1 conversionExceptionHandler){
			assertSame("Incorrect values",conversionExceptionHandler.getInfoMessage());
		}
		
	}
	
	/*
	 Test #: 2
	 Test Objective: to convert a celsius value that is greater than absolute zero
	 Test Inputs: val = -273.15
	 Expected Output: the temperature in fahrenheit will be displayed
	 */

}
